/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';
import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import type { Components } from '@seismicdev/serene/components';

import { defineCustomElement as defineMyComponent } from '@seismicdev/serene/components/my-component.js';
import { defineCustomElement as defineSrnButton } from '@seismicdev/serene/components/srn-button.js';


export declare interface MyComponent extends Components.MyComponent {}

@ProxyCmp({
  defineCustomElementFn: defineMyComponent,
  inputs: ['first', 'last', 'middle']
})
@Component({
  selector: 'my-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['first', 'last', 'middle']
})
export class MyComponent {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface SrnButton extends Components.SrnButton {}

@ProxyCmp({
  defineCustomElementFn: defineSrnButton,
  inputs: ['href', 'name', 'rel', 'shape', 'size', 'strong', 'target', 'type']
})
@Component({
  selector: 'srn-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['href', 'name', 'rel', 'shape', 'size', 'strong', 'target', 'type']
})
export class SrnButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}
