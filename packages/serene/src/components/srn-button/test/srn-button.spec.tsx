import { newSpecPage } from '@stencil/core/testing';
import { SrnButton } from '../srn-button';

describe('srn-button', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SrnButton],
      html: `<srn-button></srn-button>`,
    });
    expect(page.root).toEqualHtml(`
      <srn-button>
        <mock:shadow-root>
          <button>
          <span class="button-inner">
            <slot name="icon-only"></slot>
            <slot name="start"></slot>
            <slot></slot>
            <slot name="end"></slot>
          </span>
          </button>
        </mock:shadow-root>
      </srn-button>`);
  });
});
