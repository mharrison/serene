import { newE2EPage } from '@stencil/core/testing';

describe('srn-button', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<srn-button></srn-button>');

    const element = await page.find('srn-button');
    expect(element).toHaveClass('hydrated');
  });
});
